# Projet

## Regex (ne pas prendre en compte)
```python
r"(?P<host>[.\d]+).(?P<tiret1>\S+).(?P<tiret2>\S+).\[(?P<date>.*)\].\"(?P<request>\S+).(?P<url>\S+).(?P<protocol>\S+)\".(?P<status>\d+).(?P<size>\d+).(?P<page_internet>\".*?\").(?P<appareil>\".*?\").(?P<jesaispas2>\".*?\")"
```
## Creation colonne (ne pas prendre en compte)
```python
import re
with open("pythontest.txt") as fichier:
    with open("numbers.data", "w") as fichier2:
        for ligne in fichier:
            matching = re.match(r"(?P<host>[.\d]+).(?P<tiret1>-).(?P<tiret2>-).(?P<date>\[.*\]).(?P<request>\".*?\").(?P<status>\d+).(?P<size>\d+).(?P<page_internet>\".*?\").(?P<appareil>\".*?\").(?P<jesaispas2>\".*?\")"
, ligne)
            host, tiret1, tiret2, date, request, status, size, page_internet, appareil, jesaispas = matching.groups()       
            if matching:
                    fichier2.write(host+"\t"+date+"\t"+request+"\t"+status+"\t"+size+"\t"+page_internet+"\t"+appareil+"\n")

``` 
## Creation colonne avec librairie csv

### Combien de personnes visitent le site ?

Pour répondre à cette question nous devons utiliser la partie adresse IP de chaque ligne du log file
```python
import re
import csv

with open('access.log', 'r') as file:
    with open('propre.csv', 'w', newline='') as csvfile:
        fieldnames = ['host', 'date', 'request', 'url', 'page_internet', 'appareil']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        for ligne in file:
            matching = re.match(r"(?P<host>[.\d]+).(?P<rfc1413ident>\S+).(?P<user>\S+).\[(?P<date>.*)\].\"(?P<request>\S+).(?P<url>.*).(?P<protocol>HTTP\/\d.\d)\".(?P<status>\d+).(?P<size>\d+).\"(?P<page_internet>.*?)\".\"(?P<appareil>.*?)\".(?P<jesaispas2>\".*?\")"
, ligne)
            if matching is None:
                print(ligne)
            else:
                host, rfc1413ident, user, date, request, url, protocol, status, size, page_internet, appareil, jesaispas = matching.groups()       
                writer.writerow({'host':host, 'date':date, 'request':request, 'url':url, 'page_internet':page_internet, 'appareil':appareil})      
```
## Quels sont nos produits les plus et moins intéressants pour notre clientèle ?

```sql
SELECT url, count(*) AS compter FROM propre WHERE url LIKE '/product/%' GROUP BY url ORDER BY compter DESC;
``` 

## De quel endroit d'internet viennent les visiteurs avant d'arriver sur le site ?  

Nous avons cette information dans la colonne "referer" de notre csv. 

```sql
SELECT referer, count(*) AS compter FROM propre GROUP BY referer ORDER BY compter DESC
```
## Quels moments de la journée sont les plus/moins propices à la vente 

Pour répondre à cette question, nous avons isolé l'heure de chaque ligne du log file.

```python
with open('access_light.log', 'r') as file:
    with open('propre_light.csv', 'w', newline='') as csvfile:
        fieldnames = ['host', 'date', 'heure', 'request', 'url', 'protocol', 'page_internet', 'appareil']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        for ligne in file:
            matching = re.match(r"(?P<host>[.\d]+).(?P<rfc1413ident>\S+).(?P<user>\S+).\[(?P<date>(?P<inutile>\S+\/\S*?:)(?P<heure>\d+)(?P<reste>.*))\].\"(?P<request>\S+).(?P<url>\S+).(?P<protocol>\S+)\".(?P<status>\d+).(?P<size>\d+).(?P<page_internet>\".*?\").(?P<appareil>\".*?\").(?P<jesaispas2>\".*?\")"
, ligne)            
            if matching is None:
                print(ligne)
            else:
                host, rfc1413ident, user, date, inutile, heure, reste,  request, url, protocol, status, size, page_internet, appareil, jesaispas = matching.groups()
                writer.writerow({'host':host, 'date':date, 'heure':heure, 'request':request, 'url':url, 'protocol':protocol, 'page_internet':page_internet, 'appareil':appareil})
```
Puis une requete sql pour avoir ce que nous cherchons :

```sql
SELECT heure, count(*) AS compter FROM propre WHERE url LIKE '/product/%' GROUP BY heure ORDER BY compter DESC;
```


