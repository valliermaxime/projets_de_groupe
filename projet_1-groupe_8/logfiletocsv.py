#!/usr/bin/env python
# coding: utf-8

# In[2]:


import re
import csv                      # 2 lignes de module pour le csv et le regex
from user_agents import parse   # Sépare notre colonne user_agent pour avoir 3 colonnes: navigateur, système d'exploitation et appareil.

with open('access.log', 'r') as file:                       # Ouvre access.log et le lit
    with open('propre.csv', 'w', newline='') as csvfile:    # créer/ouvrir propre.csv pour écrir dedans
        with open('error.txt', 'w', newline='') as error:   # Créer/ouvrir error.txt pour y écrir les erreurs
            fieldnames = ['host', 'date', 'heure', 'request', 'url', 'protocol', 'referer', 'useragent', 'browser', 'os', 'device' ]                                 # Créer une variable contenant les entète de colonne
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()
            for ligne in file:
                matching = re.match(r"(?P<host>[.\d]+).(?P<rfc1413ident>\S+).(?P<user>\S+).\[(?P<date>(?P<inutile>\S+\/\S*?:)(?P<heure>\d+)(?P<reste>.*))\].\"(?P<request>\S+).(?P<url>\S+).(?P<protocol>\S+)\".(?P<status>\d+).(?P<size>\d+).(?P<referer>\".*?\").(?P<useragent>\".*?\").(?P<jesaispas2>\".*?\")"
    , ligne)            
                if matching is None:
                    error.write(ligne + "\n")   
                    #envoie les lignes qui n'ont pas match la regex dans un fichier error.txt
                else:
                    host, rfc1413ident, user, date, inutile, heure, reste,  request, url, protocol, status, size, referer, useragent, jesaispas = matching.groups()
                    #affecte les differents groupes de la regex à de variables 
                    ua_string = useragent
                    user_agent = parse(ua_string)
                    #utilisation de la fonction user_agent 
                    browser = user_agent.browser.family
                    os = user_agent.os.family
                    device = user_agent.device.family
                    writer.writerow({'host':host, 'date':date, 'heure':heure, 'request':request, 'url':url, 'protocol':protocol, 'referer':referer, 'useragent':useragent, 'browser' :browser , 'os' :os, 'device': device})
                    #ecriture ligne par ligne pour remplir les colonnes

# In[ ]:
