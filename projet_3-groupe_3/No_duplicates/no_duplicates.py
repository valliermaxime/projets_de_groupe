{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "joined-integer",
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "data = pd.read_csv('different_data.csv')\n",
    "data.drop_duplicates(subset =[\"info\",\"time\" ,\"offre\", \"Entreprise\",\"Loc\"], keep = False, inplace = True) \n",
    "data.to_csv('No_duplicates.csv', index =False)\n"
   ]
  },
  {
   "cell_type": "code",

   "id": "infectious-characteristic",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
