# Projet_3-Groupe_3

Theo, Asmaa, Maxime, Xavier, Malika

## Librairies utilisées :

# BeautifulSoup

https://www.crummy.com/software/BeautifulSoup/bs4/doc/

# Python

https://sites.google.com/view/aide-python/prise-en-main-de-python

# api pole emploi

https://www.emploi-store-dev.fr/portail-developpeur-cms/home/catalogue-des-api/documentation-des-api/api/api-offres-demploi-v2.html

# Documentation spyder

https://koor.fr/Python/Tutorial/python_ide_spyder.wp

# Doc pour le scrappping 

https://medium.com/better-programming/how-to-scrape-multiple-pages-of-a-website-using-a-python-web-scraper-4e2c641cff8

# suite
